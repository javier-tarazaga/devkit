# Blockie Ethereum DevKit

UNIX friendly command-line tool chain for building, testing and deploying Ethereum-based applications.


## Requirements

* Space.sh. https://space.sh  
* Docker

For platform-specific information about installation, please refer to [INSTALL instructions](INSTALL.md).


## Create a new project
```sh
space -m web3-sh/devkit /init/ -e project=summon_nick_johnson -e template=raiseforacause
```

Projects start off based on the _dapp_ `template` project, provided with the _DevKit_.  
All projects are stored locally, by default, in `${HOME}/superdevkit/projects` directory (`projectshome`).


## Run the project locally
The _DevKit_ provides local execution of any projects by using Docker as the default container engine:  
```sh
space -m web3-sh/devkit /run/
```

This will output some information in the console.  
A web server (`nginx`) will run in the background. Access it by browsing to `http://127.0.0.1`, where `127.0.0.1` is the _Docker IP address_.   
_Hint: if you are not sure what the IP is, try the Docker Machine default: `192.168.99.100`_  

In case port `80` is already taken, it is possible to customize it by doing:  

```sh
space -m web3-sh/devkit /run/ -e port=8080
```  
Other ports are: `rpcport=8545` (geth RPC), `wsport=8546` (geth WebService), `remixdport=65520` (RemixD) 

Custom host and port configuration requires different _Cross-Origin Resource Sharing (CORS)_ settings, which can be customized with the `-e cors` and `-e corsip` options.  

### Stopping the container
The `/stop/` command gracefully shuts down the _DevKit_ container:  
```sh
space -m web3-sh/devkit /stop/
```


## Remix
Access the _Remix IDE_ visiting `http://127.0.0.1/remix`, where `127.0.0.1` is the _Docker IP_.  
You can connect _Remix_ to your local _geth_ node to deploy contracts directly from the _IDE_.  

This can be done by changing the _Environment_ setting in the _Run_ tab.  
By default, the _JavaScript VM_ setting will be selected, resulting in operations being run in a simulated environment provided by _Remix IDE_.  
Other options are _Injected Web3_, which connects to third-party services and _Web3 Provider_, which enables connecting to an existing node.

Select _Web3 Provider_ and proceed confirming a connection to an _Ethereum_ node. Edit the _Web3 Provider Endpoint_ to match the local _geth_ node address e.g. `http://127.0.0.1:8545`. Click _OK_ and the connection will be established.  

The default `genesis.json` file that configures the _Ethereum_ node contains multiple accounts preallocated with _ether_. They will be available in the _Account_ list in the _Run_ tab inside _Remix_.  

The _Devkit_ shares the local `superdevkit` projects directory (by default located in `$HOME/superdevkit/projects`) with the _IDE_ using _Remixd_. In order to enable communication between the _IDE_ and _Remixd_, a connection must be started by clicking the fifth button on the top left corner of the screen, named "localhost connection". A dialog will pop up asking for confirmation. Press _Connect_ and the icon will turn green. After that, the file hierarchy, located at the left side panel, will show a new item named "localhost". Upon expanding the "localhost" item, it will be possible to explore, open, edit and delete project files directly from the _IDE_.

In order to add an existing source file to the _IDE_ manually, click in the _Add Local file to the Browser Storage Explorer_ on the top left corner and browse to your local `superdevkit` projects directory, by default located in the `$HOME/superdevkit/projects`. Select `RaiseToSummon.sol` file and confirm. The file will now be loaded into the _IDE_.

## MyEtherWallet
This is how you configure the local _MEW_ to access your local _geth_ _Ethereum_ node.

Go to `http:///127.0.0.1/myetherwallet/`, where `127.0.0.1` is the _Docker IP_. On the top right corner, click on _Network_ list and then _Add Custom Node_. Fill in the data fields accordingly:  
* Node Name: `Blockie`
* URL: `http://127.0.0.1`
* Port: `8545`
* Select Custom

Click _Save & Use Custom Node_ button. If everything is OK, a connection message will appear at the bottom of the page:  
```
You are successfully connected
```


## Block Explorer
Access the _Block Explorer_ visiting `http://127.0.0.1/explorer`, where `127.0.0.1` is the _Docker IP_.  
The _Explorer_ enables inspecting the _Ethereum_ blockchain data, including transactions, accounts and blocks. The page automatically updates showing new blocks and latest transactions.


## Accounts
The `genesis.json` file that configures the _Ethereum_ node contains multiple accounts preallocated with _ether_. Each account is permanently unlocked inside _geth_ client.

To list all the accounts which _geth_ is managing:  
```sh
space -m web3-sh/devkit /account/list/
```


## Transfer funds
To transfer funds from any of the genesis accounts to any other account which you may have created in _MyEtherWallet_:  

```sh
space -m web3-sh/devkit /account/transfer/ -e from=0x8833af1a9b1e7ddc837ed2c771078dcf3ac23daf -e to=0x620cbab1f950e38a964d02ddcf85ecfcbb9f468f -e value=101010101010101
```

The `from` account you can get from the accounts listing above.

`value` is in _wei_.


## Convert values: handling _ether_ and _wei_
The `/convert/` operation can help in the process of transforming _ether_ and _wei_ values.

From _ether_ to _wei_:  
```sh
space -m web3-sh/devkit /convert/towei/ -e value=1
```  
Output:  
```sh
1000000000000000000
```

From _wei_ to _ether_:  
```sh
space -m web3-sh/devkit /convert/toether/ -e value=1
```  
Output:
```sh
0.000000000000000001
```


## Console

For other operations and administrative tasks on the _Ethereum_ node, open a _JavaScript_ console in _geth_:  
```sh
space -m web3-sh/devkit /console/
```

## Compile
The _Devkit_ can compile all newly created or updated contracts in a project.
```sh
space -m web3-sh/devkit /compile/ -e project=summon_nick_johnson
```

The generated `bin` and `abi` files will be located in the project directory: `${HOME}/superdevkit/projects/summon_nick_johnson`   
It is possible to toggle `optimization` by setting the option to `true` or `false`.  

The `container` option can be overridden for executing commands locally instead of using a container: `-e container=""`.

### Recompile all

By default, compilation only happens when either:  
1. a given source file hasn't been compiled yet, or   
2. when a file has been updated.  
In order to trigger a complete `recompile` of all project files, it is necessary to set the `-e recompile=true` option.  
Alternatively, for recompiling a single source file that has not been changed, it is possible to touch it either by saving again or running the `touch` command.


## Compile with constructor parameters (add arguments)

Use `/add_args/` to compile only a specific file, appending the constructor arguments to the generated binary file. Example:  
```sh
space -m web3-sh/devkit /add_args/ -e project=summon_nick_johnson -e contract=RaiseToSummon -- 0x00Ee7508D62B152C3413C55807052644714ba36D 'Let us summon Nick' 3600
```

This option helps in situations that require the complete set of binary data for signing a transaction, such as when deploying a contract with _MyEtherWallet_.


## Test
It is possible to perform automated testing for one or all of the contracts within a project.
```sh
space -m web3-sh/devkit /test/ -e project=summon_nick_johnson
```

To test a single file:
```sh
space -m web3-sh/devkit /test/ -e project=summon_nick_johnson -e contract=RaiseToSummon
```

The `hotreload` option enables watching for test file changes and repeat the tests automatically for faster iterations:
```sh
space -m web3-sh/devkit /test/ -e project=summon_nick_johnson -e contract=RaiseToSummon -e hotreload=true
```

The `container` option can be overridden for executing commands locally instead of using a container: `-e container=""`.


## Build the project
The _DevKit_ can deploy any contract to the blockchain running inside the container.

This is the same syntax as for the previous steps (`/compile/` and `/test/`), except there is an option to add constructor arguments.
```sh
space -m web3-sh/devkit /build/ -e project=summon_nick_johnson -- 0x00Ee7508D62B152C3413C55807052644714ba36D 'Let us summon Nick' 3600
```

The positional arguments, appended at the end of the command, define the constructor arguments for the contract.  
The `container` option can be overridden for executing commands locally instead of using a container: `-e container=""`.

On screen there will be `stderr` messages and on `stdout` the contract address(es).

The project directory will now have a directory named `deploy`, where it will keep all the different projects and environments. In this example, it is expected to find a file with the `.addr` extension, located in `deploy/local/`. The `addr` file contains the newly deployed contract address.

### Rebuild all

By default, project build only happens when either:  
1. a given compiled contract hasn't been deployed yet, or   
2. when a contract binary has been changed  
In order to trigger a complete `rebuild` of all project contracts, it is necessary to set the `-e rebuild=true` option.  


## Retrieve transaction receipt
Given a transaction hash from `/build/` step, it is possible to read the receipt with the following command:
```sh
space -m web3-sh/devkit /get_receipt/ -e tx=0x828f360e2b11654422e51256c2f9d10a5814f049f25482d3e13ecddd1799a05e
```


## Build JavaScript objects

The _JavaScript_ representation of the contracts for a given environment can be built with the `/js/` command.

```sh
space -m web3-sh/devkit /js/ -e project=summon_nick_johnson
```

By default, following the _dapp_ template project, a `contracts.js` file will be generated and stored in `www/js`. That makes the contracts automatically accessible in the _dapp_ project.


## The complete cycle with `/make/`
`/make/` performs all steps involved for getting a project from local source files to deployed binaries. That includes `/compile/`, `/build/` and `/js/` in a single command.

```sh
space -m web3-sh/devkit /make/ -e project=summon_nick_johnson
```

All the options specific to each step are also available when running `/make/`, including `optimization`, forcing `recompile` and `rebuild`.


## Projects
All projects are listed in `http://127.0.0.1/projects`, where `127.0.0.1` is the _Docker IP_.  
DApps are accessible right from the browser. Following the example _summon_nick_johnson_ project example, it is possible to access it browsing to: `http://127.0.0.1/projects/summon_nick_johnson/`.


## Docker image
Images are automatically built as part of version releases.  
The latest image can be downloaded with the following command:  
```
docker pull registry.gitlab.com/web3-sh/devkit:latest
```

For older versions a complete list can be found at GitLab: https://gitlab.com/web3-sh/devkit/container_registry
