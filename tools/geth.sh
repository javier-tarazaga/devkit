#!/usr/bin/env bash
#
# Copyright 2017 Blockie AB
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Arguments: "cors" "apis" "extraargs to geth" "main dir of geth installation"

log()
{
    local level="${1}"
    shift
    local msg="${1}"
    shift
    printf "[%s]: %s\n" "${level}" "${msg}" >&2
}

# The chrome-extension is MetaMask.
cors="${1:-"http://192.168.99.100,http://192.168.99.100:8080,http://127.0.0.1:8080,http://localhost:8080,http://superdevkit:8080,http://127.0.0.1,http://localhost,http://superdevkit,chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn"}"
shift $(( $# > 0 ? 1 : 0 ))

apis="${1:-"eth,personal,web3,admin,net,txpool,debug"}"
shift $(( $# > 0 ? 1 : 0 ))

extraargs="${1-}"
shift $(( $# > 0 ? 1 : 0 ))

dir="${1:-"/home/devkit/geth"}"
shift $(( $# > 0 ? 1 : 0 ))

cd "${dir}" || {
    log "ERROR" "Could not find directory ${dir}"
    exit 1
}

geth init --datadir="${dir}" genesis.json || {
    log "ERROR" "Geth could not init the chain. If you changed the genesis.json, then you will need to destroy and re-run this container to clear out the old chain. All your transactions and deployed contracts will ofc be destroyed in the process."
    exit 1
}

keystore="${dir}/keystore"
cd "${keystore}" || {
    log "ERROR" "Keystore dir not found: ${keystore}"
    exit 1
}
log "INFO" "Extracting accounts from ${keystore}"

unlock=""
accounts=""
for keyfile in UTC*; do
    [[ ${keyfile} =~ UTC--.+--(.+)$ ]]
    unlock="${unlock:+${unlock},}${BASH_REMATCH[1]}"
    accounts="${accounts}0x${BASH_REMATCH[1]}
"
done

log "INFO" "These accounts have a lot of ether and are unlocked inside geth. The first account is also the PoA signer:
"
printf "%s" "${accounts}"

printf "\n" >&2

# Already checked: assume it is a valid dir at this point
# shellcheck disable=2164
cd "${dir}"

gethcmd="geth --nodiscover --port 30303 --mine --unlock='${unlock}' --password=./keystore/passw.txt --datadir='${dir}' --rpc --rpcaddr=0.0.0.0 --rpcapi='${apis}' --ws --wsaddr=0.0.0.0 --wsapi='${apis}' --rpccorsdomain='${cors}' ${extraargs}"
log "CMD" "${gethcmd}"

eval "${gethcmd}"
