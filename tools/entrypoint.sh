#!/usr/bin/env bash
#
# Copyright 2017 Blockie AB
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

cd /home/devkit/ && node node_modules/remixd/ -S /home/devkit/projects &
cd /home/devkit/explorer && npm start &
cd /home/devkit/tools

# Arguments are passed on to geth.sh
./nginx.sh &&
./geth.sh "$@"
