#!/usr/bin/env sh
#
# Copyright 2017 Blockie AB
#
# This file is part of web3-sh/devkit.
#
# web3-sh/devkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 3 of the License.
#
# web3-sh/devkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with web3-sh/devkit  If not, see <http://www.gnu.org/licenses/>.
#

#
# Bump devkit version
#

set -o errexit
set -o nounset


#
# Check arguments
if [ "$#" -lt 1 ]; then
    printf "Missing version name. Example: $0 1.0.0\n" >&2
    exit 1
fi

#
# Check requirements
if ! command -v sed >/dev/null; then
    printf "Missing sed program. Exiting...\n" >&2
    exit 1
fi

#
# Files to change
_changelog_file="./CHANGELOG.md"
_module_file="./Spacefile.yaml"
_version_file="./space-module.txt"

#
# Data
_current_version_name=$(grep -m 1 "image_version" ${_module_file} | cut -d':' -f2 | cut -d ' ' -f2)
_version_name="$1"
_version_date=$(date "+%Y-%m-%d")

# Update files
sed -i.bak "s/\[current\]/\[${_version_name} - ${_version_date}\]/" "$_changelog_file"
sed -i.bak "s/${_current_version_name}/${_version_name}/g" "$_module_file"
sed -i.bak "s/${_current_version_name}/${_version_name}/g" "$_version_file"

#
# Cleanup temporary files
if [ -f "$_changelog_file" ]; then
    rm "${_changelog_file}.bak"
fi

if [ -f "$_module_file" ]; then
    rm "${_module_file}.bak"
fi

if [ -f "$_version_file" ]; then
    rm "${_version_file}.bak"
fi
