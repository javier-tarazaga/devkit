#!/usr/bin/env sh
#
# Copyright 2017 Blockie AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

_test_entrypoint="./test.yaml"

#
# Check the file exists in current or parent directory
if [ ! -f "${_test_entrypoint}" ]; then
    _test_entrypoint="./test/test.yaml"
fi

space -f "${_test_entrypoint}" /_tests/ -a -a -v4 -L5 -C0
