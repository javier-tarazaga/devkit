#
# Copyright 2017 Blockie AB
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

_TEST_DEVKIT_ACCOUNT_LIST()
{
    SPACE_DEP="DEVKIT_ACCOUNT_LIST PRINT"

    local output=""
    output=$(DEVKIT_ACCOUNT_LIST "$1")

    local expected_output="0x8833af1a9b1e7ddc837ed2c771078dcf3ac23daf
0x620cbab1f950e38a964d02ddcf85ecfcbb9f468f
0x671f2db52e03634b4100c6bbfbfc01c26ac19b63
0x919c6a8966b0f137d8bc8e9c6979e2cb8ef29e2f
0xd0cfc0a5d683630731e088488500b3cede5d1f2a
0x5c85efa7e09b63e1581e266ebe812c0e78e627a2
0x4f73ed954acd185a5d88bb87db1a2f8e152539c5"

    if [ "${expected_output}" = "${output}" ]; then
        PRINT "OK: success (${output})" "ok"
        exit 0
    else
        PRINT "Failed." "error"
        PRINT "Expected: ${expected_output}" "error"
        PRINT "Got:      ${output}" "error"
        exit 1
    fi
}

_TEST_DEVKIT_ACCOUNT_TRANSFER()
{
    # shellcheck disable=2034
    SPACE_DEP="DEVKIT_ACCOUNT_TRANSFER PRINT"

    local output=""
    output=$(DEVKIT_ACCOUNT_TRANSFER "$1" "$2" "$3" "$4")
    output=$(printf "%s" "${output}" | sed "s/\"//g")

    if [ "${output}" != "${output#0x}" ]; then
        PRINT "OK: success (${output})" "ok"
        exit 0
    else
        PRINT "Failed." "error"
        PRINT "Got:      ${output#0x}" "error"
        PRINT "Got:      ${output}" "error"
        exit 1
    fi
}
