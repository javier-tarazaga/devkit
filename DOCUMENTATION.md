#  | [![build status](https://gitlab.com/space-sh/devkit/badges/master/build.svg)](https://gitlab.com/space-sh/devkit/commits/master)


## /account/
	

+ list
+ transfer

## /add_args/
	Append arguments to existing binary 

	Run the compiler within the container to append
	constructor arguments to a contract in a specific project.
	Export SOLC and GETH variables to overwrite defaults.
	


## /build/
	Build the smart contracts to the blockchain(s) for a given environment

	Build all smart contracts as defined in the Dappfile.yaml to the blockchain(s) network(s)
	defined by the given environment.
	


## /compile/
	Compile a project's smart contracts

	Compile all (updated) contracts for a project as defined in the Dappfile.yaml.
	


## /console/
	Open a geth console attached to the container


## /convert/
	

+ toether
+ towei

## /get_receipt/
	Read a transaction receipt


## /init/
	Create a new DApp project from a template git repository

	Clones a git repository and removes the .git folder to uninitilize the repository.
	If no repository URI is given, then it copies the built-in dapp template.
	


## /js/
	Build the JavaScript representation of the contracts for a given environment

	Build the contracts.js according the the Dappfile.yaml
	defined by the given environment.
	


## /make/
	Compile, build and build js for project

	Run all three targets sequentially: /compile/, /build/ and /js/
	


## /run/
	Run Devkit container


## /stop/
	Stop Devkit container


## /test/
	Test a smart contract

	Run tests for one or more contracts inside the container for a specific project.
	


# Functions 

## DEVKIT\_DEP\_INSTALL()  
  
  
  
Check dependencies for this module.  
  
### Returns:  
- 0: dependencies were found  
- 1: failed to find dependencies  
  
  
  
## DEVKIT\_PROJECT\_INIT()  
  
  
  
Initialize a new project  
  
### Parameters:  
- $1: template directory  
- $2: projects directory  
- $3: project name  
  
### Expects:  
- MODULEDIR  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_RUN\_CONTAINER()  
  
  
  
Run Docker container  
  
### Parameters:  
- $1: container image  
- $2: container name  
- $3: projects directory  
- $4: web server port  
- $5: RPC port  
- $6: WS port  
- $7: remixd port  
- $8: CORS policy (optional)  
- $9: genesis configuration file (optional)  
- $10: path to nginx directory (optional)  
  
### Returns:  
- Non-zero on failure.  
  
  
  
## DEVKIT\_STOP\_CONTAINER()  
  
  
  
Stop Docker container  
  
### Parameters:  
- $1: container name  
  
### Returns:  
- Non-zero on failure.  
  
  
  
## DEVKIT\_CONSOLE()  
  
  
  
Open console to geth  
  
### Parameters:  
- $1: Geth IPC address  
  
### Expects:  
- GETH to point to go-ethereum, defaults to "geth".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_ACCOUNT\_TRANSFER()  
  
  
  
Transfer funds from one account to another.  
  
### Parameters:  
- $1: ipc address  
- $2: From address  
- $3: To address  
- $4: value in wei  
  
### Expects:  
- GETH to point to go-ethereum, defaults to "geth".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_ACCOUNT\_LIST()  
  
  
  
List available accounts.  
  
### Parameters:  
- $1: ipc address  
  
### Expects:  
- GETH to point to go-ethereum, defaults to "geth".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_COMPILE\_PROJECT()  
  
  
  
Compile a smart contract.  
  
### Parameters:  
- $1: projects home directory  
- $2: project name  
- $3: optimization flag  
- $4: recompile flag  
  
### Expects:  
- SOLC to point to solidity compiler, defaults to "solc".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## \_DEVKIT\_COMPILE\_ETHEREUM()  
  
  
  
Compile for Ethereum  
  
### Expects:  
- SOLC to point to solidity compiler, defaults to "solc".  
  
  
  
## DEVKIT\_BUILD\_PROJECT()  
  
  
  
Build a smart contract to the network.  
  
### Parameters:  
- $1: Geth IPC address  
- $2: projects home directory  
- $3: project name  
- $4: blockchain environment name  
- $5: rebuild flag  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## \_DEVKIT\_DAPPFILE\_JS()  
  
  
List all js representations of contracts.  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
- $2: the name of the environment we are working on  
  
### Returns:  
- Lines on stdout.  
  
  
  
## \_DEVKIT\_DAPPFILE\_CONTRACT\_POSTDEPLOY()  
  
  
  
List all post deploy entries for a contract and its associated data line by line.  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
- $2: the name of the environment we are working on  
- $3: the index of the contract to get post deployments for  
  
### Returns:  
- Lines on stdout.  
  
  
  
## \_DEVKIT\_DAPPFILE\_CONTRACTS()  
  
  
  
List all contracts and their associated data line by line.  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
- $2: the name of the environment we are working on (optional).  
- $3: the index of the contract to get, -1 means all.  
  
### Returns:  
- Lines on stdout.  
  
  
  
## \_DEVKIT\_DAPPFILE\_CONTRACT\_POSTDEPLOY\_ARGS()  
  
  
  
List all arguments for a contracts post deploys  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
- $2: Contract index  
- $3: Post Deploy index  
  
### Returns:  
- Lines on stdout.  
  
  
  
## \_DEVKIT\_DAPPFILE\_CONTRACT\_ARGS()  
  
  
  
List all arguments for a contract  
  
### Parameters:  
- $1: Filepath of Dappfile.yaml  
- $2: Contract index  
  
### Returns:  
- Lines on stdout.  
  
  
  
## \_DEVKIT\_BUILD()  
  
  
  
Inner implementation of build  
  
### Parameters:  
- $1: projects home directory  
- $2: deployment directory  
- $3: backup directory  
- $4: blockchain name  
- $5: network name  
- $6: source file  
- $7: contract file  
- $8: account  
- $9: address  
- $10: gas amount  
- $11: IPC address  
- $12: rebuild flag  
- $13: Contract index  
- $14: arguments  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_BUILD\_ETHEREUM\_LOCAL()  
  
  
  
Deploy to local Ethereum geth node via ipc.  
  
### Parameters:  
- $1: projects home directory  
- $2: deployment directory  
- $3: backup directory  
- $4: source file  
- $5: contract file  
- $6: account  
- $7: address  
- $8: gas amount  
- $9: IPC address  
- $10: rebuild flag  
- $11: Contract index  
- $12: arguments  
  
### Expects:  
- GETH  
- \_deploy\_pending: array  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_ETHEREUM\_COUNT\_FN\_ARGS()  
  
  
  
Count function arguments  
  
### Parameters:  
- $1: source file  
- $2: contract file  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_ETHEREUM\_ADD\_ARGS()  
  
  
  
Add arguments  
  
### Parameters:  
- $1: IPC address  
- $2: ABI file  
- $3: bin file  
- $4: arguments  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_BUILD\_ETHEREUM\_LOCAL\_CB()  
  
  
  
Callback function to check if a contract has been deployed.  
  
### Parameters:  
- $1: line  
  
### Expects:  
- $id: id of user  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_POST\_DEPLOY()  
  
  
  
Process post deployment  
  
### Parameters:  
- $1: IPC address  
- $2: projects home directory  
- $3: project name  
- $4: environment name  
  
### Expects:  
- $id: id of user  
- $\_deploy\_done  
  
### Returns:  
- Non-zero on error  
  
  
  
## \_DEVKIT\_BUILD\_WAIT()  
  
  
  
Wait for build step to complete  
  
### Parameters:  
- None  
  
### Expects:  
- $id: id of user  
- $\_deploy\_pending  
- $\_deploy\_done  
  
### Returns:  
- Non-zero on error  
  
  
  
## DEVKIT\_JS\_BUILD\_PROJECT()  
  
  
  
### Parameters:  
- $1: projects home directory  
- $2: project name  
- $3: environment name  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_TEST()  
  
  
  
Test a contract  
  
### Parameters:  
- $1: projects home directory  
- $2: project name  
- $3: contract file name  
- $4: hot reload flag  
- $5: break on first failure flag  
  
### Expects:  
- TESTER path to tester binary  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_GET\_RECEIPT()  
  
  
  
Read a transaction receipt  
  
### Parameters:  
- $1: Geth node IPC endpoint  
- $2: receipt hash  
  
### Expects:  
- GETH to point to go-ethereum, defaults to "geth".  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_CONVERT\_TO\_ETHER()  
  
  
  
Convert to ether  
  
### Parameters:  
- $1: value  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_CONVERT\_TO\_WEI()  
  
  
  
Convert to wei  
  
### Parameters:  
- $1: value  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## DEVKIT\_ADD\_ARGS()  
  
  
  
Append arguments to existing compiled binary file  
This operation considers that arguments are always  
appended the same way and not packed differently in  
optimized builds.  
  
### Parameters:  
- $1: IPC address  
- $2: projects home directory  
- $3: project name  
- $4: contract file name  
- $@: arguments  
  
### Expects:  
- SOLC  
- GETH  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## \_DEVKIT\_QUOTE\_ARG()  
  
  
  
Helper function for escaping and quoting arguments in place  
  
### Parameters:  
- $1: variable to quote  
  
### Returns:  
- Non-zero on failure  
  
  
  
## \_DEVKIT\_PARSE\_DAPPFILE()  
  
DEVKIT\_PARSE\_DAPPFILE  
  
Parse Dapp configuration file  
  
### Parameters:  
- None  
  
### Returns:  
- Non-zero on failure  
  
  
  
## DEVKIT\_MAKE\_PROJECT()  
  
  
  
Perform all steps needed to completely make a project  
  
### Parameters:  
- $1: projects home directory  
- $2: project name  
- $3: optimization flag  
- $4: recompile flag  
- $5: IPC address  
- $6: environment name  
- $7: rebuild flag  
  
### Returns:  
- Non-zero on failure  
  
  
  
