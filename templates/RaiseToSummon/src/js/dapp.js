(function() {
    var web3;
    var instance;

    function gotWeb3(web3) {
        this.web3 = web3;
        $("#loading").hide();
        $(".metamask").show();
        var eth = new Eth(web3.currentProvider);
        var contract = new EthContract(eth);
        var abi=contracts['RaiseToSummon'].abi;
        var address=contracts['RaiseToSummon'].address;
        instance = contract(abi);
        instance = instance.at(address);
        console.log(instance.donate);

        $("#donatebtn").click(function() {
            donate();
        });
    }

    function donate() {
        if(this.web3.eth.accounts.length==0) {
            alert("There are no accounts loaded in Metamask. Unlock wallet and reload page.");
            return false;
        }
        instance.donate({from: this.web3.eth.accounts[0], value: new BN(1000), gas: "99000", gasPrice:'33000000000'}).then(function() {
            console.log(result);
            alert('Thank you for your donation!');
        }).catch(function(a,b) {
            alert('The donation was cancelled.');
        });
    }

    function lackingWeb3() {
        $("#loading").hide();
        $(".nometamask").show();
        $("button.reqmetamask").attr('disabled', true);
    }

    $(document).ready(function() {
        if (typeof window.web3 !== 'undefined') {
            gotWeb3(window.web3);
        } else {
            lackingWeb3();
        }
    });
})();
