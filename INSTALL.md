# _DevKit_ installation instructions


## Requirements

* _Space.sh_ (version 1.1.0 or later): https://space.sh

* _Docker_: https://docker.com


## Installing Space.sh

On the _GNU/Linux_ or equivalent command line, enter:
```
curl -s https://get.space.sh | sudo sh
```


## Installing Docker

Visit https://www.docker.com/community-edition#download and download _Docker Community edition (CE)_ for your platform.

For more details regarding Docker install, check the latest documentation:  
* Linux: https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/
* Mac: https://docs.docker.com/docker-for-mac/install/


## Windows-specific instructions

_Space.sh_ requires _Bash_ version 3.2 or later under a _GNU/Linux_ environment or equivalent. It is possible to setup such an environment under _Windows 10_ by taking advantage of _Windows Subsystem for Linux_ (WSL).

### Enable Microsoft Windows Subsystem for Linux (WSL)
Run _Windows PowerShell_ as administrator and enter the following command:
```
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```

Wait for the command line to complete. It will ask for a restart. Answer "Y"


### Install Ubuntu

1. Visit: http://aka.ms/wslstore and choose _Ubuntu_ distribution

2. Open the command prompt and type in:
```
ubuntu
```
and wait for installation to succeed.

3. After installation has finished, it will prompt for an account name and password.
Commands `bash` and `ubuntu` will be available from there on via the _Windows_ command prompt.


### Install Space.sh

1. Enter _Ubuntu bash_ for Windows
2. Install _Space.sh_ in _Ubuntu_:
```
curl -s https://get.space.sh | sudo sh
```

### Install _Docker Toolbox_ (for _Windows 10 Home edition_ or CPUs without _HyperV_)
Regular _Docker_ can be downloaded from https://www.docker.com.  
For _Windows Home edition_ and other targets that do not support _HyperV_, use _Docker Toolbox_: https://download.docker.com/win/stable/DockerToolbox.exe


1. Enter _Ubuntu bash_ for _Windows_ subsystem for _Linux_ by typing in `bash` in the command prompt
2. Install _Docker_ client in _Ubuntu_:
```
sudo apt update
sudo apt install docker.io
```

3. Configuration depends on host _Docker_ environment settings. On _Windows_ prompt call:
```
docker-machine env default
```

4. Copy the output adjusting to shell export:
```
export DOCKER_TLS_VERIFY=1
export DOCKER_HOST=tcp://192.168.99.100:2376
export DOCKER_CERT_PATH=/mnt/c/Users/<myusername>/.docker/machine/certs
```

_Docker_ commands are expected to work from here on.

#### Configuring firewall settings for _Docker_

In order to configure open ports, it is necessary to run _Windows Defender Firewall_. Right click `Inbound Rules` and add a new rule. Select `port`, TCP and specific port set to `8080`. Proceed until the name section and hit Finish.
 
For _Docker Toolbox_, configure the _VM_ accordingly. In _Virtual Box_, under `Settings/Network/Adapter`, add a new rule in the `Port Forwarding Rules` panel.


## Running the _DevKit_

On the command line, enter:
```sh
space -m web3-sh/devkit /run/
```

This will output some information in the console.  
A web server (`nginx`) will run in the background. Access it by browsing to `http://127.0.0.1`, where `127.0.0.1` is the _Docker IP address_.   
_Hint: if you are not sure what the IP is, try the Docker Machine default: `192.168.99.100`_  
In case port `80` is already taken, it is possible to customize it by doing:  

```sh
space -m web3-sh/devkit /run/ -e port=8080
```  

For more information, please refer to the README file located in the project repository.
